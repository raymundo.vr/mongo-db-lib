use anyhow::Result;
use mongodb::{options::ClientOptions, Client, Database};

pub async fn initialize(mongodb_uri: &str, database: &str) -> Result<Database> {
    let options = ClientOptions::parse(mongodb_uri).await?;
    let client = Client::with_options(options)?;
    let db = client.database(database);
    Ok(db)
}

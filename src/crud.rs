use anyhow::Result;
use core::marker::{Send, Sync, Unpin};
use futures::TryStreamExt;
use mongodb::results::InsertOneResult;
use mongodb::{
    bson::document::Document,
    options::{FindOneAndUpdateOptions, FindOneOptions, FindOptions, UpdateModifications},
    Database,
};
use serde::{de::DeserializeOwned, Serialize};

pub trait MongoDbModel {
    fn collection_name() -> String;
}

pub async fn create<I: Serialize + MongoDbModel>(
    instance: &I,
    database: &Database,
) -> Result<InsertOneResult> {
    let collection = database.collection::<I>(&I::collection_name());
    let insert_result = collection.insert_one(instance, None).await?;
    Ok(insert_result)
}

pub async fn find_one<I>(
    db: &Database,
    filter: Option<Document>,
    options: Option<FindOneOptions>,
) -> Option<I>
where
    I: MongoDbModel + DeserializeOwned + Unpin + Send + Sync,
{
    let col = db.collection::<I>(&I::collection_name());
    let doc = match col.find_one(filter, options).await {
        Ok(doc) => doc,
        Err(_) => return None,
    };

    doc
}

pub async fn find_many_as_vec<I>(
    db: &Database,
    filter: Option<Document>,
    options: Option<FindOptions>,
) -> Vec<I>
where
    I: MongoDbModel + DeserializeOwned + Unpin + Send + Sync,
{
    let col = db.collection::<I>(&I::collection_name());
    let mut cursor = match col.find(filter, options).await {
        Ok(cursor) => cursor,
        Err(_) => return vec![],
    };
    let mut documents: Vec<I> = Vec::new();
    while let Ok(Some(doc)) = cursor.try_next().await {
        documents.push(doc);
    }
    documents
}

pub async fn update_one<I>(
    db: &Database,
    query: Document,
    update: UpdateModifications,
    options: Option<FindOneAndUpdateOptions>,
) -> Option<I>
where
    I: MongoDbModel + DeserializeOwned + Unpin + Send + Sync,
{
    let col = db.collection::<I>(&I::collection_name());
    match col.find_one_and_update(query, update, options).await {
        Ok(result) => result,
        Err(_) => None,
    }
}

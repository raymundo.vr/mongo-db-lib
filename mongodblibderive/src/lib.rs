mod model;
mod attribute;

use proc_macro::TokenStream;
use syn::{AttributeArgs, ItemStruct, parse_macro_input};
use crate::model::impl_mongo_derive;
use crate::attribute::impl_lib_attribute;

#[proc_macro_derive(MongoDB, attributes(mongodb))]
pub fn mongodb_derive(input: TokenStream) -> TokenStream {
    impl_mongo_derive(input)
}

#[proc_macro_attribute]
pub fn mongodb_collection(args: TokenStream, input: TokenStream) -> TokenStream {
    let attrs = parse_macro_input!(args as AttributeArgs);
    let mut input = parse_macro_input!(input as ItemStruct);

    impl_lib_attribute(attrs, input)
}
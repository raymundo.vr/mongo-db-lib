use darling::FromMeta;
use syn::{AttributeArgs, ItemStruct, parse::Parser};
use proc_macro::TokenStream;
use quote::quote;

#[derive(FromMeta)]
pub struct MongoDbCollectionArgs {
    pub name: String,
    pub timestamps: Option<bool>,
    pub auto_id: Option<bool>,
}

pub(crate) fn impl_lib_attribute(args: AttributeArgs, mut input: ItemStruct) -> TokenStream {
    let args = match MongoDbCollectionArgs::from_list(&args) {
        Ok(values) => values,
        Err(e) => return TokenStream::from(e.write_errors())
    };

    let MongoDbCollectionArgs{ name, timestamps, auto_id } = args;
    let ident = &input.ident;

    if let syn::Fields::Named(ref mut fields) = input.fields {
        if let Some(_add_timestamps) = timestamps {
            fields.named.push(
                syn::Field::parse_named
                    .parse2(quote! {
                        #[serde(with = "bson::serde_helpers::chrono_datetime_as_bson_datetime")]
                        pub created_at: chrono::DateTime<chrono::Utc> 
                    })
                    .unwrap()
            );
        }

        if let Some(add_id) = auto_id {
            println!("Adding Id");
        }
    }

    let gen = quote! {
        #input

        impl MongoDbModel for #ident {
            fn collection_name() -> String {
                #name.to_string()
            }
        }
    };

    gen.into()
}
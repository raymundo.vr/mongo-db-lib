use darling::FromDeriveInput;
use proc_macro::TokenStream;
use syn::{DeriveInput, Ident, parse};
use quote::quote;


#[derive(FromDeriveInput)]
#[darling(attributes(mongodb))]
struct MongoDBOptions {
    ident: Ident,
    collection: String,
}

pub(crate) fn impl_mongo_derive(input: TokenStream) -> TokenStream {
    let ast: DeriveInput = parse(input).expect("Error deriving MongoDB");
    let MongoDBOptions {ident, collection} = FromDeriveInput::from_derive_input(&ast).expect("Error deriving MongoDB");
    
    let gen = quote! {
        impl MongoDbModel for #ident {
            fn collection_name() -> String {
                #collection.to_string()
            }
        }
    };

    gen.into()
}
use mongodblib_derive::MongoDB;
use mongodblib::crud::*;

#[derive(MongoDB)]
#[mongodb(collection="derived")]
struct DeriveModel;

#[test]
fn derive_collection_name() {
    let name = DeriveModel::collection_name();
    assert_eq!(name, "derived");
}
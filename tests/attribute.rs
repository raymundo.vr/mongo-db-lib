use mongodblib_derive::mongodb_collection;
use mongodblib::crud::*;
use serde::Serialize;

#[mongodb_collection(name="by_attribute")]
struct MyCollection {
    field: String,
}

#[test]
fn test_collection_name() {
    let name = MyCollection::collection_name();
    assert_eq!(name, "by_attribute");
}

#[derive(Serialize)]
#[mongodb_collection(name="with_timestamps", timestamps)]
struct MyCollectionWithTimestamps {
    field: String,
}

#[test]
fn test_collection_timestamps() {
    let timestamp = chrono::Utc::now();
    let my_data = MyCollectionWithTimestamps { field: "Hey".to_string(), created_at: timestamp };
    assert_eq!(my_data.created_at, timestamp);
}